import { Router, Request, Response } from "express";
import { saveUser, listUser } from "./controller/UserController";

const routes = Router();

routes.get("/", (request: Request, response: Response) =>
  response.json({ message: "Hello Node" })
);

routes.post('/user', saveUser)
routes.get('/users', listUser)

export default routes;
