import { getRepository } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { User } from "../entity/User";
import * as bcrypt from "bcrypt";

export const listUser = async (request: Request, response: Response) => {
  const user = await getRepository(User).find();
  return response.json(user);
};

export const saveUser = async (request: Request, response: Response) => {
  const { name, email, password } = request.body;

  const passwordHash = await bcrypt.hash(password, 8);

  const user = await getRepository(User).save({
    name,
    email,
    password: passwordHash,
  });

  return response.json(user);
};
